﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class LevelUnlock : MonoBehaviour
{
    public Button[] levelButtons;
    public Sprite UnlockImage;
    

    // Start is called before the first frame update
    void Start()
    {
            
    }

    // Update is called once per frame
    void Update()
    {

        // DeleteData(true);
        
        
        OnLevelUnlock();

            
    }

    public void OnLevelUnlock()
    {

            for (int i = 0; i <=PlayerPrefs.GetInt("Level",0); i++)
            {
            Debug.Log(PlayerPrefs.GetInt("Level"));

            levelButtons[i].interactable = true;
                levelButtons[i].GetComponentInChildren<Text>().text = (i+1).ToString();
                levelButtons[i].image.sprite = UnlockImage;
            }
        
    }

    private void DeleteData(bool del)
    {

        if (del)
        {
            PlayerPrefs.DeleteAll();
        }
        else
            return;

    }

    private void Awake()
    {
        
    }

}
