﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skybox : MonoBehaviour
{
    public Material[] materials;

    // Start is called before the first frame update
    void Start()
    {
        Material skyBoxMaterial;
        skyBoxMaterial = materials[Random.Range(0, materials.Length)];
        RenderSettings.skybox = skyBoxMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
