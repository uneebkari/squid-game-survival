﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audiomanager : MonoBehaviour
{
    public static Audiomanager Instance;
    public AudioSource audioSource,bgmAudioSource;


    public AudioSource audioClip;
    public AudioClip bgSound;
    public AudioClip redLightEffect,shot,playerDeath;

    private void Awake()
    {
        if(!Instance)
        {
            Instance = this;
            //    DontDestroyOnLoad(gameObject);
            //}else
            //{
            //    DestroyImmediate(gameObject);
        }

    }

    private void Start()
    {
        //audioSource.volume = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void playSound(AudioClip audioClip)
    {
        audioSource.PlayOneShot(audioClip);
        //audioSource.loop = true;
    }
    //public void playSound()
    //{
    //    bgmAudioSource.PlayOneShot(bgSound);
    //    bgmAudioSource.loop = true;
    //}

}
