﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
   public static PlayerController instance;

    //ahsan
    public static bool adCalled = false;
    public static int counter = 0;


    public Joystick joystick;
    public CharacterController controller;
    public Animator anim;
    public Image bulletIndicator;
    public Transform targetIndicator;
    public Transform target;
   // public ParticleSystem movementEffect;
    public ParticleSystem shootingEffect;
    public GameObject bloodEffect;

    public Transform gunFront;
    public GameObject bullet;
    AudioSource audio;
    public float speed;
    public float gravity;
    public float bulletCount;
    public float reloadSpeed;
    public GameObject DeathFx;
    Vector3 moveDirection;
    float bullets = 1f;
    public Joystick findJoystick;
    bool reloading;
  //  GameObject LivePlayer;
    List<GameObject> bulletStorage = new List<GameObject>();


    [HideInInspector]
    public bool safe;
    public GameObject redLight;

    void Start()
    {
        instance = this;

       // GameObject.FindGameObjectWithTag("Boss");
        // manager = GameObject.FindObjectOfType<GameManager>();
        //  audio = GameObject.Find("ObstacleAudio").GetComponent<AudioSource>();
        
        //ahsan
        //findJoystick = GameObject.Find("Joystick").GetComponent<Joystick>();
      
        //  LivePlayer = GameObject.Find("LivePlayer");

        adCalled = false;
    }


    void OnEnable()
    { //or Awake() or Start()
        controller.enabled = true;
    }

    void Update()
    {

       // counter = GameObject.FindGameObjectsWithTag("Player").Length;

        if(RedLighGreenLightGameManager.instance.joystickActivate == true)
        {


            Vector2 direction = findJoystick.direction;


            moveDirection = new Vector3(direction.x * 0.8f, 0, direction.y);

            Quaternion targetRotation = moveDirection != Vector3.zero ? Quaternion.LookRotation(moveDirection) : transform.rotation;
            transform.rotation = targetRotation;

            moveDirection = moveDirection * speed;


            moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
            controller.Move(moveDirection * Time.deltaTime);



            if (anim.GetBool("Moving") != (direction != Vector2.zero))
            {
                anim.SetBool("Moving", direction != Vector2.zero);

                if (direction != Vector2.zero)
                {
                    // movementEffect.Play();

                }
                else
                {
                    //   movementEffect.Stop();

                }
            }


        }
        if (RedLighGreenLightGameManager.instance.isGameStartedFirstTime && redLight.activeInHierarchy && flag && anim.GetBool("Moving")==true) {
            flag = false;
            anim.SetBool("isDead",true);
            Audiomanager.Instance.playSound(Audiomanager.Instance.playerDeath);
            Audiomanager.Instance.playSound(Audiomanager.Instance.shot);
            Debug.Log("Please Kill Me");
            Invoke("ShowGameOver", 1f);

        }

    }
    bool flag = true;
    /* public void Fire(){
         GameObject newBullet = bulletStorage.Count > 0 ? recycleBullet() : Instantiate(bullet);

         newBullet.transform.rotation = transform.rotation;
         newBullet.transform.position = gunFront.position;

         Bullet bulletController = newBullet.GetComponent<Bullet>();
         bulletController.player = this;
         shootingEffect.Play();
     }
     */
    public void SwitchSafeState(bool safe)
    {
        this.safe = safe;

        targetIndicator.gameObject.SetActive(!safe);
    }

    public void Die()
    {
        Instantiate(bloodEffect, transform.position + Vector3.up * 1.5f, transform.rotation);
        Destroy(gameObject);
    }

    GameObject recycleBullet()
    {
        GameObject newBullet = bulletStorage[0];
        bulletStorage.Remove(newBullet);
        newBullet.SetActive(true);

        return newBullet;
    }

    public void DisableBullet(GameObject targetBullet)
    {
        targetBullet.SetActive(false);
        bulletStorage.Add(targetBullet);
    }

    public void OnTriggerEnter(Collider collision)
    {
        if (!enabled)
            return;
        
            //if (collision.tag.Equals("NPCs"))
            //{

            //    collision.gameObject.GetComponent<Animator>().SetBool("isFall", true);
            //    collision.gameObject.GetComponent<Animator>().SetBool("isMoving", false);
                
                
            //    // Only used for NPC players
            //    // this.gameObject.GetComponent<Animator>().SetBool("isFall", false);
            //    // this.gameObject.GetComponent<Animator>().SetBool("isMoving", true);

            //}
        if (collision.gameObject.tag == "Finish") {
            anim.SetBool("isWin", true);
            controller.enabled = false;
            Invoke("ShowGameOver", 3f);
        }
        
    }
    void ShowGameOver() {
        GamePlayUIManager.instance.GameOver();

    }

}
