﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class RedLighGreenLightGameManager : MonoBehaviour
{
    public static RedLighGreenLightGameManager instance;

    public GameObject Joystick;
    public GameObject greenLight, redLight;
    public GameObject player;
    public GameObject[] NPCs;

    public int whatIsTheDecision;


    public Text alertMsg;


    public bool joystickActivate = false;
    public bool isGameStartedFirstTime = true;
    public bool isRedLighGreenLightStarted=false;
     public bool isAlive = true;
    public bool isSoundPlay = false;
    
    // old flag value not in use for a moment
    //public bool flagValue = false;

    private void Awake()
    {
        NPCs = GameObject.FindGameObjectsWithTag("NPCs");
    }


    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        joystickActivate = false;
        Joystick.SetActive(false);


            

        //NPC flag value coroutine
       
    }

    // Update is called once per frame
    void Update()
    {

        Debug.Log("GameManager is " + gameObject.name);


        if (isAlive)
        {
            // red light green light functionality
            if (greenLight.activeInHierarchy)
            {
                //Audiomanager.Instance.audioSource.volume = 1;
                
                

                alertMsg.text = "Go";
                NPCs = GameObject.FindGameObjectsWithTag("NPCs");
                if (NPCs != null)
                {
                    foreach (var item in NPCs)
                    {
                        item.GetComponent<Npc_Ai>().iSNpcDecidedToMove = true;

                        //ahsan states
                        item.GetComponent<Animator>().SetBool("isMoving", true);
                        item.GetComponent<Animator>().speed = 1;

                    }
                }
                

              

            }
            else if (redLight.activeInHierarchy)
            {
                NPCs = GameObject.FindGameObjectsWithTag("NPCs");

                if (NPCs != null)
                {
                    foreach (var item in NPCs)
                    {
                        //ahsan states
                        //item.GetComponent<Animator>().SetBool("isMoving", false);



                        switch (item.GetComponent<Npc_Ai>().whatIsTheDecision)
                        {
                            case 0:
                                item.GetComponent<Npc_Ai>().iSNpcDecidedToMove = false;
                                break;
                            case 1:
                                if (item.GetComponent<Animator>().GetBool("isFall") == false)
                                {
                                    item.GetComponent<Npc_Ai>().iSNpcDecidedToMove = true;
                                    item.GetComponent<Animator>().SetBool("isDead", true);
                                }
                                break;
                            default:
                                Debug.LogError("Invalid decision");
                                break;
                        }

                    }

                }
                
            }
        }

        

        /*if (player.GetComponent<Animator>().GetBool("Moving") == true)
        {
            alertMsg.text = "Player died";
        }
        else
        {
            alertMsg.text = "Stop";
        }*/







    }



    public void StartGame()
    {
        StartCoroutine(StartTheGame());

        StartCoroutine(ColorLightChange_Coroutine());
    }

    IEnumerator StartTheGame()
    {
        yield return new WaitForSeconds(5f);
        Joystick.SetActive(true);
        joystickActivate = true;
        player.GetComponent<Animator>().SetBool("GameStarted",true);

        isAlive = true;

      //  Audiomanager.Instance.playSound(Audiomanager.Instance.bgSound);
        Audiomanager.Instance.audioSource.volume = 1;

        //NPCs[0].GetComponent<Animator>().SetBool("isMoving", true);
        foreach (var item in NPCs)
        {
            item.GetComponent<Animator>().SetBool("isMoving", true);
        }

        //  NPCs[1].GetComponent<Animator>().SetBool("isMoving", true);




        //Activate all the Npc Players
        NpcActive();
        
        PlayerController.instance.findJoystick =GameObject.Find("Joystick").GetComponent<Joystick>();

       // StartCoroutine(flagValueCounter());
    }


    public void NpcActive()
    {
        
        foreach (var item in NPCs)
        {
            item.GetComponent<Npc_Ai>().iSNpcDecidedToMove = true;
           
        }


        }


 

    //change the npc move or stop state
    IEnumerator flagValueCounter()
    {
        
        
        while(isRedLighGreenLightStarted)
        {
            yield return new WaitForSeconds(3f);

            Debug.Log("Flag changed");


            foreach (var item in NPCs)
            {
                int whatIsTheDecision = Random.Range(0, 2);

                switch (whatIsTheDecision)
                {
                    case 0:
                        item.GetComponent<Npc_Ai>().iSNpcDecidedToMove = false;
                        break;
                    case 1:
                        if (item.GetComponent<Animator>().GetBool("isFall") == false)
                        {
                            item.GetComponent<Npc_Ai>().iSNpcDecidedToMove = true;
                        }
                        break;
                    default:
                        Debug.LogError("Invalid decision");
                        break;
                }


            }



        }

    }






    IEnumerator ColorLightChange_Coroutine()
    {
        while (isAlive)
        {
            

            

            if (greenLight.activeInHierarchy)
            {
                
                yield return new WaitForSeconds(5f);


                Audiomanager.Instance.audioSource.volume = 0;
                isSoundPlay = false;


                greenLight.SetActive(false);
                redLight.SetActive(true);
                Debug.Log("red light");
            }
            else if (redLight.activeInHierarchy)
            {
                yield return new WaitForSeconds(5f);

                if (!isSoundPlay)
                {
                    Audiomanager.Instance.audioSource.Play();
                    Audiomanager.Instance.audioSource.volume = 1;
                   // Audiomanager.Instance.playSound(Audiomanager.Instance.bgSound);
                    isSoundPlay = true;
                }


                redLight.SetActive(false);
                greenLight.SetActive(true);

                Debug.Log("green light");
            }
        }

    }




}
