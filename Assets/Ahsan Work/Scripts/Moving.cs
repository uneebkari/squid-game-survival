﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : NpcBaseFSM
{

    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

         

        //agent.isStopped = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
       // if (GameManager.instance.flagValue)
        if (NPC.GetComponent<Npc_Ai>().iSNpcDecidedToMove)
        {
            animator.speed = 1;
            agent.isStopped = false;
            agent.SetDestination(Target.position);
            //animator.speed = 1f;
        }
        else
        { 
            agent.isStopped = true;
            animator.speed = 0;
            // animator.speed = 0f;

        }

        /* if (animator.GetComponent<Npc_Ai>().redLight.activeInHierarchy == true)
         {
             if (animator.GetComponent<Npc_Ai>().chosen)
             {
                 agent.isStopped = true;
                 animator.speed = 0;
             }
             else
             {
                 animator.speed = 1;
                 agent.isStopped = false;
                 agent.SetDestination(Target.position);
             }

         }
         else
         {


             agent.isStopped = false;
             agent.SetDestination(Target.position);
         }*/



    }
     
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
