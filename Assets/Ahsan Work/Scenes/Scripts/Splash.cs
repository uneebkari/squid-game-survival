﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour {

	void Start () {
		StartCoroutine (LoadMenu ());
	}

	IEnumerator LoadMenu(){
	
		yield return new WaitForSeconds (1.5f);
		SceneManager.LoadScene ("Menu");
	}

	public void OpenGamePolygon(){

		Application.OpenURL ("www.gamepolygon.com");
	}

}
