﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelection : MonoBehaviour
{
    public DataBase _data;
    
    
    public GameObject[] AllLevels;


    private void OnEnable()
    {
        TapdaqAdsManager.instance.LoadRewardedAd();
        HighlightLastLevel();
    }


    void HighlightLastLevel()
    {
        ResetAllLevels();
        
        for (int i = 0; i < AllLevels.Length; i++)
        {
            if (i < _data.UnlockedLevel)
            {
                AllLevels[i].transform.GetChild(1).gameObject.SetActive(false);
                
                if(i==_data.UnlockedLevel-1)
                {
                    AllLevels[i].transform.GetChild(3).gameObject.SetActive(true);
                    AllLevels[i+1].transform.GetChild(2).gameObject.SetActive(true);    
                }
                
                
            }

           
        }
    }

    public void OnLevelButtonClick(int levelNumber)
    {
        Constant.CurrentSelectedLevel = levelNumber;
        GameManager.instance.LoadGamePlayScene();
    }


    void ResetAllLevels()
    {
        foreach (var level in AllLevels)
        {
            level.transform.GetChild(1).gameObject.SetActive(true);
            level.transform.GetChild(2).gameObject.SetActive(false);
            level.transform.GetChild(3).gameObject.SetActive(false);
        }
    }

    public void ShowRewardedAd()
    {
        TapdaqAdsManager.instance.LoadandShowRewardedAd();
    }
}
