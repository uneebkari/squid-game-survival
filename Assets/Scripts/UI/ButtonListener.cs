﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonListener : MonoBehaviour 
{
    public Menus MenuType;
    public Popups PopupType;
    
    public void MenuClick()
    {
        UIManager.instance.LoadMenu(MenuType);
    }
    
    public void PopupClick()
    {
        UIManager.instance.LoadPopup(PopupType);
    }

   
}
