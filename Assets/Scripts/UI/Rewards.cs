﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rewards : MonoBehaviour
{

    public GameObject[] days;

    private void OnEnable()
    {
        
    }

    public void GetRewards(int day)
    {
        CoinCounter.instance.UpdateCoins(day*250);
        UIManager.instance.DisplayMessageBox(MessageBoxType.Reward, ("You earned reward of : " + day * 250).ToString(),false,false,true);
        gameObject.SetActive(false);
    }
}
