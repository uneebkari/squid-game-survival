﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum MessageBoxType
{
    Quit,
    Purchase,
    NotEnoughCoins,
    Reward
}
public class MessageBox : MonoBehaviour
{

    public MessageBoxType Type;

    public Text MessageArea;

    public GameObject Yes, No, Close;
    
    public void ActionButtonYes()
    {
        if (Type == MessageBoxType.Quit)
        {
            Application.Quit();
        }

        if (Type == MessageBoxType.Purchase)
        {
            gameObject.SetActive(false);
        }
    }

    public void ActionButtonOK_Close()
    {
        if (Type == MessageBoxType.Reward)
        {
            CoinCounter.instance.UpdateCoins(1000);
        }
        gameObject.SetActive(false);
    }


    public void MessageDisplayText(MessageBoxType type,string txt,bool yes,bool no,bool close)
    {
        Type = type;
        Yes.SetActive(false);
        No.SetActive(false);
        Close.SetActive(false);
        
        MessageArea.text = txt;
        
        if(yes)
            Yes.SetActive(true);
        if(no)
            No.SetActive(true);
        if(close)
            Close.SetActive(true);
    }
}
