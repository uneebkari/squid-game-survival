﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CloseOnClick : MonoBehaviour,IPointerClickHandler
{
    
    public void OnPointerClick(PointerEventData eventData)
    {
        UIManager.instance.LoadPopup(Popups.None);
    }
}
