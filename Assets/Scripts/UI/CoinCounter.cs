﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinCounter : MonoBehaviour
{
    public static CoinCounter instance;


    public Text CoinsText;
    
    private void Awake()
    {
        instance = this;
    }

    private void OnEnable()
    {
        CoinsText.text = Referance.instance._Data.Coins.ToString();
    }

    public void UpdateCoins(int Number)
    {
        if(!isUpdating)
            StartCoroutine(CounterUpdate(Number));
    }

    private int i = 0;
    private bool isUpdating;
    IEnumerator CounterUpdate(int number)
    {
        isUpdating = true;
        i = 0;
        while (i<=number)
        {
            CoinsText.text = (Referance.instance._Data.Coins + i).ToString();
            yield return new WaitForSeconds(0.001f);
            i++;
        }

        isUpdating = false;
        Referance.instance._Data.Coins += number;
        yield return null;
    }
}
