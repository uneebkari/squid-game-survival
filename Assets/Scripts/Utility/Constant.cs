﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ModeType
{
    ChallangeMode,
    FreeRom
}

public class Constant
{
    public static int CurrentSelectedLevel;
    public static int TotalLevels;
}
