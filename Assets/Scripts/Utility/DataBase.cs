﻿using UnityEngine;

[CreateAssetMenu(fileName = "DataBase", menuName = "ScriptableObjects/DataBase", order = 1)]
public class DataBase : ScriptableObject
{
    public int Coins;
    public int UnlockedLevel;
}
