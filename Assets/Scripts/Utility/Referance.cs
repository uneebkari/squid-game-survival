﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Referance : MonoBehaviour
{
   public static Referance instance;

   private void Awake()
   {
      if(instance!=null)
         Destroy(gameObject);
      else
         instance = this;
      
      DontDestroyOnLoad(this);
   }


   public DataBase _Data;
}
