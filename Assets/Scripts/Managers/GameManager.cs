﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    #region Instancing

    public static GameManager instance;

    private void Awake()
    {
        
        if(instance!=null)
            Destroy(gameObject);
        else
            instance = this;

        DontDestroyOnLoad(this);

    }
    #endregion

    private AsyncOperation _operation;
    private bool isLoading;

    public int MenuNumber,GamePlayNumber;
    
    
    public void LoadGamePlayScene()
    {
        if(!isLoading)
            StartCoroutine(Load(GamePlayNumber));
    }
    
    public void LoadMainMenu()
    {
        if(!isLoading)
            StartCoroutine(Load(MenuNumber));
    }

    IEnumerator Load(int id)
    {
        isLoading = true;
        UIManager.instance.LoadLoadingScreen(true);
        _operation = SceneManager.LoadSceneAsync(id);

        while (!_operation.isDone)
        {
            yield return null;
        }
        UIManager.instance.LoadLoadingScreen(false);
        isLoading = false;
        if(id == GamePlayNumber)
        {
            UIManager.instance.ActiveDeavtiveTopBar(false);
            UIManager.instance.LoadMenu(Menus.None);
            UIManager.instance.LoadPopup(Popups.HUD);
            TapdaqAdsManager.instance.LoadBannerAd();
        }
        else
        {
            UIManager.instance.ActiveDeavtiveTopBar(true);
            UIManager.instance.LoadMenu(Menus.MainMenu);
            UIManager.instance.LoadPopup(Popups.None);
            TapdaqAdsManager.instance.HideBanner();
        }
    }
}
