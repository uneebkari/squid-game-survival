﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Menus
{
    None,
    MainMenu,
    LevelSelection,
    PlayerSelection,
    ModeSelection
}

public enum Popups
{
    None,
    Pause,
    Settings,
    LevelComplete,
    LevelFail,
    Rewards,
    Inapp,
    HUD
}


public class UIManager : MonoBehaviour
{
    #region Instance

    public static UIManager instance;

    private void Awake()
    {
       
        if(instance!=null)
            Destroy(gameObject);
        else
            instance = this;

        DontDestroyOnLoad(this);

    }


    #endregion
    
    [Serializable]
    public class UIMenus
    {
        public string Name;
        public GameObject Menu;
        public Menus Type;
    }
    
    [Serializable]
    public class PopUp
    {
        public string Name;
        public GameObject Popup;
        public Popups Type;
    }


    public UIMenus[] _Menus;
    public PopUp[] _Popups;

    private UIMenus _currentMenu;
    private PopUp _currentPopup;

    public GameObject TopBar;
    public GameObject LoadingScreen;

    public GameObject Message;

    public void LoadMenu(Menus type)
    {
        foreach (var _men in _Menus)
        {
            _men.Menu.SetActive(false);
        }

        foreach (var _men in _Menus)
        {
            if (_men.Type == type)
            {
                _currentMenu = _men;
                _men.Menu.SetActive(true);
            }
        }
    }
    
    public void LoadPopup(Popups ty)
    {
        foreach (var _pop in _Popups)
        {
            _pop.Popup.SetActive(false);
        }

        foreach (var _pop in _Popups)
        {
            if (_pop.Type == ty)
            {
                _currentPopup = _pop;
                _pop.Popup.SetActive(true);
                BannerAd_OnlyOn_Hud();
                LoadAd();
            }
        }
    }

    public void LoadLoadingScreen(bool active)
    {
        LoadingScreen.SetActive(active);
    }
    
    public void MessagePopup(string Text, bool active)
    {
        Message.SetActive(active);
    }

    public void BackButtonClick()
    {
        switch (_currentMenu.Type)
        {
            case Menus.None:
                break;
            case Menus.MainMenu:
                    DisplayMessageBox(MessageBoxType.Quit,"Do you want to Quit ?",true,true,false);
                break;
            case Menus.LevelSelection:
                    LoadMenu(Menus.ModeSelection);
                break;
            case Menus.PlayerSelection:
                LoadMenu(Menus.LevelSelection);
                break;
            case Menus.ModeSelection:
                LoadMenu(Menus.MainMenu);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void ActiveDeavtiveTopBar(bool value)
    {
        TopBar.SetActive(value);
    }

    public void Start()
    {
        LoadMenu(Menus.MainMenu);
        ActiveDeavtiveTopBar(true);
    }

    public void LoadMainMenu()
    {
        GameManager.instance.LoadMainMenu();
    }
    public void LoadGamePlay()
    {
        GameManager.instance.LoadGamePlayScene();
    }

    public void DisplayMessageBox(MessageBoxType typ, string txt, bool yes, bool no, bool close)
    {
        Message.SetActive(true);
        Message.GetComponent<MessageBox>().MessageDisplayText(typ,txt,yes,no,close);
    }
    
    public void LoadAd()
    {
        if (_currentPopup.Type == Popups.Pause || 
            _currentPopup.Type == Popups.LevelComplete ||
            _currentPopup.Type == Popups.LevelFail)
            
            TapdaqAdsManager.instance.LoadandShowInterstitial();
        
    }

    public void ShowRewardedAd()
    {
        TapdaqAdsManager.instance.LoadandShowRewardedAd();
    }

    public void BannerAd_OnlyOn_Hud()
    {
        if(_currentPopup.Type == Popups.HUD)
        {
            TapdaqAdsManager.instance.LoadandShowBannerAd();
            //AdsManager.instance.RequestBannerAd();
        }
        else
        {
            TapdaqAdsManager.instance.HideBanner();
            //AdsManager.instance.DestroyBannerAd();
        }
        
    }

}
