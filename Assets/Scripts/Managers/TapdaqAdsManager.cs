﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tapdaq;



public class TapdaqAdsManager : MonoBehaviour
{
   public static TapdaqAdsManager instance;

   private void Awake()
   {
      if (instance != null)
         Destroy(instance);
      else
         instance = this;
      
      DontDestroyOnLoad(this);
   }

   public string placementID = "default";
   public TDBannerPosition BannerPosition;
   public TDMBannerSize BannerSize;


   private void Start()
   {
      AdManager.Init();
   }
   
   private void OnEnable () {
      TDCallbacks.TapdaqConfigLoaded += OnTapdaqConfigLoaded;
      TDCallbacks.TapdaqConfigFailedToLoad += OnTapdaqConfigFailToLoad;
      TDCallbacks.AdAvailable += OnAdAvailable;
      TDCallbacks.RewardVideoValidated += OnRewardVideoValidated;
   }

   private void OnDisable () {
      TDCallbacks.TapdaqConfigLoaded -= OnTapdaqConfigLoaded;
      TDCallbacks.TapdaqConfigFailedToLoad -= OnTapdaqConfigFailToLoad;
      TDCallbacks.AdAvailable -= OnAdAvailable;
      TDCallbacks.RewardVideoValidated -= OnRewardVideoValidated;
   }

   private void OnTapdaqConfigLoaded()
   {
      TapdaqAdsManager.instance.LoadRewardedAd();
      //Tapdaq Ready to use
   }

   private void OnTapdaqConfigFailToLoad(TDAdError error) {
      //Tapdaq failed to initialise
   }
   
   private void OnAdAvailable(TDAdEvent e) 
   {
      if (e.IsInterstitialEvent() && e.tag == placementID) 
      {
        // AdManager.ShowInterstitial("default");
      }
      
      if (e.IsBannerEvent()) 
      {
         if (AdManager.IsBannerReady(placementID)) 
         {
            ShowBanner();
         }
      }
      if (e.IsVideoEvent() && e.tag ==placementID) 
      {
         //AdManager.ShowVideo(placementID);
      }
      if (e.IsRewardedVideoEvent() && e.tag == placementID) 
      {
       //  AdManager.ShowRewardVideo(placementID);
      }
   }

   public void LoadandShowBannerAd()
   {
      AdManager.RequestBanner(BannerSize, placementID);
   }

   void ShowBanner()
   {
      if (AdManager.IsBannerReady(placementID)) 
      {
         AdManager.ShowBanner(BannerPosition, placementID);
      }
      else
      {
         LoadandShowBannerAd();
      }
   }

   public void HideBanner()
   {
      AdManager.HideBanner(placementID);
   }


   public void LoadandShowInterstitial()
   {
      if (AdManager.IsInterstitialReady("default")) 
      {
         AdManager.ShowInterstitial(placementID);
      }
      else
      {
         AdManager.LoadInterstitial(placementID);   
      }
      
   }

   public void LoadandShowVideoInterstitial()
   {
      if (AdManager.IsVideoReady(placementID)) 
      {
         AdManager.ShowVideo(placementID);
      }
      else
      {
         AdManager.LoadVideo(placementID);   
      }
      
   }

   public void LoadandShowRewardedAd()
   {
      if (AdManager.IsRewardedVideoReady(placementID))
      {
         AdManager.ShowRewardVideo(placementID);
      }
      else
      {
         AdManager.LoadRewardedVideo(placementID);      
      }
   }

   public void LoadBannerAd()
   {
      AdManager.RequestBanner(BannerSize, placementID);

   }

   public void LoadIntersitialAd()
   {
      AdManager.LoadVideo(placementID);   
      AdManager.LoadInterstitial(placementID);   
   }

   public void LoadRewardedAd()
   {
      AdManager.LoadRewardedVideo(placementID);
   }

   private void OnRewardVideoValidated(TDVideoReward videoReward)
   {
      Debug.Log ("I got " + videoReward.RewardAmount + " of " + videoReward.RewardName 
                 + "   tag=" + videoReward.Tag + " IsRewardValid " + videoReward.RewardValid + " CustomJson: " + videoReward.RewardJson);
			
      if(videoReward.RewardValid) 
      {
         //Give Reward
         UserReward();
      }
      else
      {
         UserNotRewardDiplayRewardError();
         //Reward is invalid, video may not have completed or an ad network may have refused to the provide reward
      }
   }

   void UserReward() // Reward User Here
   {
      CoinCounter.instance.UpdateCoins(1000);
      UIManager.instance.DisplayMessageBox(MessageBoxType.Reward, ("You earned reward of : " + 1000).ToString(),false,false,true);
   }

   void UserNotRewardDiplayRewardError()
   {
      
   }

   void AdsNotAvalible()
   {
      UIManager.instance.DisplayMessageBox(MessageBoxType.Reward, ("Ads Not Avalible : ").ToString(),false,false,true);
   }
}
