﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerUneeb : MonoBehaviour
{

    public static AudioManagerUneeb instance = null;
     AudioSource audioSource;

    public AudioClip click, shot, levelFailed, LevelCompleted;
    void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void PlayThisClip(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
