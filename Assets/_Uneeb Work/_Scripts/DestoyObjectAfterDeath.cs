﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoyObjectAfterDeath : MonoBehaviour
{
    public void DestroyThis() {
        Destroy(this.gameObject);
        GamePlayUIManager.instance.canCountOponents = true;
    }
}
