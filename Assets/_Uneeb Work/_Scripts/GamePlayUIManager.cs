﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GamePlayUIManager : MonoBehaviour
{
    public static GamePlayUIManager instance;
    public GameObject rulesPanel, gameOverPanel,loadingPanel;
    public Text totalNoOfPlayers, remainingNumberofPlayers;

    public bool canCountOponents=false;
    public void HideRules() {
        if (AudioManagerUneeb.instance != null)
            AudioManagerUneeb.instance.PlayThisClip(AudioManagerUneeb.instance.click);
        rulesPanel.SetActive(false);

    }

    public void StartTheGame() {
    }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        totalNoOfPlayers.text = GameObject.FindGameObjectsWithTag("NPCs").Length.ToString();
        remainingNumberofPlayers.text = (GameObject.FindGameObjectsWithTag("NPCs").Length + 1).ToString();

    }

    // Update is called once per frame
    void Update()
    {
        if (canCountOponents)
        {

            remainingNumberofPlayers.text = (GameObject.FindGameObjectsWithTag("NPCs").Length + 1).ToString();
            canCountOponents = false;
        }
    }
    public void GameOver() {
        //AdController.instance.ShowRewardedAd();
        gameOverPanel.SetActive(true);
    }
    public void Retry() {
        if(AudioManagerUneeb.instance!=null)
            AudioManagerUneeb.instance.PlayThisClip(AudioManagerUneeb.instance.click);
        //AdController.instance.ShowAd();

        loadingPanel.SetActive(true);
        Invoke("LoadSameScene",5f);
    }
    public void Home()
    {
        if (AudioManagerUneeb.instance != null)
            AudioManagerUneeb.instance.PlayThisClip(AudioManagerUneeb.instance.click);
        loadingPanel.SetActive(true);
        Invoke("LoadMainMenu", 5f);
        //AdController.instance.ShowAd();

    }
    void LoadSameScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
    void LoadMainMenu() {
        SceneManager.LoadScene(0);

    }

}
