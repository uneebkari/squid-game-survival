﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MainMenuUIManager : MonoBehaviour
{

    public GameObject mainMenu, modeSelection, loading;
  

    public void GoToModeSelction() {
        AudioManagerUneeb.instance.PlayThisClip(AudioManagerUneeb.instance.click);
        mainMenu.SetActive(false);
        modeSelection.SetActive(true);
        loading.SetActive(true);
        //AdController.instance.ShowAd();
        Invoke("DisableLoading", 5f);
    }
    void DisableLoading() {
        loading.SetActive(false);
    }
    public void BackToMainMenu() {
        mainMenu.SetActive(true);
        modeSelection.SetActive(false);
        loading.SetActive(true);
        AudioManagerUneeb.instance.PlayThisClip(AudioManagerUneeb.instance.click);
        //AdController.instance.ShowAd();

        Invoke("DisableLoading", 5f);

    }
    public void RateUs() {
        AudioManagerUneeb.instance.PlayThisClip(AudioManagerUneeb.instance.click);
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
    }
    public void StartRedGreenLightGame() {
        //AdController.instance.ShowAd();

        AudioManagerUneeb.instance.PlayThisClip(AudioManagerUneeb.instance.click);
        loading.SetActive(true);
        Invoke("DisableLoading", 5f);
        Invoke("LoadGameScene",5f);
    }
    void LoadGameScene() {
        SceneManager.LoadScene(1);
    }
    // Start is called before the first frame update
    void Start()
    {
        loading.SetActive(true);
        Invoke("DisableLoading", 5f);
        Invoke("ShowStartAd", 5f);

    }
    void ShowStartAd() {
        //AdController.instance.ShowAd();

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
